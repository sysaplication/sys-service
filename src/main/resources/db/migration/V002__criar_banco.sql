
-- Table: situacao

-- DROP TABLE situacao;

CREATE TABLE situacao(
    situacao_id bigint NOT NULL,
    descricao character varying(25),
    CONSTRAINT situacao_pkey PRIMARY KEY (situacao_id)
);

-- Table: uf

-- DROP TABLE uf;

CREATE TABLE uf (
  uf_id     bigserial NOT NULL,
  nome   varchar(60),
  sigla  varchar(2),
  ibge   integer,
  pais   integer,
  CONSTRAINT uf_pkey PRIMARY KEY (uf_id)
);

COMMENT ON TABLE uf
  IS 'Unidades Federativas';


-- Table: pessoa

-- DROP TABLE pessoa;

CREATE TABLE pessoa
(
    pessoa_id bigint NOT NULL,
    nome character varying(70) NOT NULL,
    situacao_id bigint NOT NULL,
    celular character varying(12),
    uf_id bigint,
    data_nascimento timestamp NOT NULL,
    CONSTRAINT pessoa_pkey PRIMARY KEY (pessoa_id),
    CONSTRAINT ct_pessoa_situacao FOREIGN KEY (situacao_id)
        REFERENCES situacao (situacao_id),
    CONSTRAINT ct_pessoa_uf FOREIGN KEY (uf_id)
        REFERENCES uf (uf_id)
);