package br.com.sys.feature.uf;

import br.com.sys.core.repository.SysRepository;
import br.com.sys.shared.model.UF;

interface UFRepository extends SysRepository<UF, Long> {
    
}