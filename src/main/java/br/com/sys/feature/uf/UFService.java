package br.com.sys.feature.uf;

import br.com.sys.core.service.GenericService;
import br.com.sys.shared.model.UF;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Service
@Transactional
public class UFService extends GenericService<UF, Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
}
