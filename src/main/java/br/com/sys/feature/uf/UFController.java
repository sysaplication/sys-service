package br.com.sys.feature.uf;

import br.com.sys.shared.dto.PessoaDTO;
import br.com.sys.shared.dto.SituacaoDTO;
import br.com.sys.shared.dto.UFDTO;
import br.com.sys.shared.mapper.SituacaoMapper;
import br.com.sys.shared.mapper.UFMapper;
import br.com.sys.shared.model.Pessoa;
import br.com.sys.shared.model.UF;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/ufs")
class UFController {

    private final UFService uFService;

    private final UFMapper uFMapper;

    public UFController(UFService UFService, UFMapper uFMapper) {
        this.uFService = UFService;
        this.uFMapper = uFMapper;
    }

    @GetMapping
    public Page<UFDTO> recuperarTodos(final Pageable pageable) {
        final Page<UF>  page = uFService.recuperarTodos(pageable);
        return new PageImpl<>((List<UFDTO>) uFMapper.toDTO(page.getContent()), pageable, page.getTotalElements());
    }

}