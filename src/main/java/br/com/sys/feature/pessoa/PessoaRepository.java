package br.com.sys.feature.pessoa;

import br.com.sys.core.repository.SysRepository;
import br.com.sys.shared.model.Pessoa;
import br.com.sys.shared.model.Situacao;

import java.util.Optional;

interface PessoaRepository extends SysRepository<Pessoa, Long> {

}