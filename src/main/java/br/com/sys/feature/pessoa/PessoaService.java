package br.com.sys.feature.pessoa;

import br.com.sys.core.service.GenericService;
import br.com.sys.shared.model.Pessoa;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.startsWith;

@Service
@Transactional
public class PessoaService extends GenericService<Pessoa, Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private final PessoaRepository pessoaRepository;

	public PessoaService(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}

	public Page<Pessoa> recuperarTodosPorTermo(String termo, Pageable pageable) {

		Pessoa person = new Pessoa();
		person.setNome(termo);
		ExampleMatcher matcher = ExampleMatcher.matching()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
				.withIgnoreCase();
		Example<Pessoa> example = Example.of(person, matcher);

		return this.pessoaRepository.findAll(example, pageable);
	}
}
