package br.com.sys.feature.pessoa;

import br.com.sys.shared.dto.PessoaDTO;
import br.com.sys.shared.dto.SituacaoDTO;
import br.com.sys.shared.mapper.PessoaMapper;
import br.com.sys.shared.mapper.SituacaoMapper;
import br.com.sys.shared.model.Pessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/pessoas")
class PessoaController {

    private final PessoaService pessoaService;

    private final PessoaMapper pessoaMapper;

    public PessoaController(PessoaService pessoaService, PessoaMapper pessoaMapper) {
        this.pessoaService = pessoaService;
        this.pessoaMapper = pessoaMapper;
    }

    @PostMapping
    public void salvar(@RequestBody PessoaDTO pessoaDTO) {
        pessoaService.salvarOuAlterar(pessoaMapper.toEntity(pessoaDTO));
    }

    @PutMapping
    public void alterar(@RequestBody PessoaDTO pessoaDTO) {
        pessoaService.salvarOuAlterar(pessoaMapper.toEntity(pessoaDTO));
    }

//    @GetMapping
//    public Page<PessoaDTO> recuperarTodos(final Pageable pageable) {
//        final Page<Pessoa>  page = pessoaService.recuperarTodos(pageable);
//        return new PageImpl<>((List<PessoaDTO>) pessoaMapper.toDTO(page.getContent()), pageable, page.getTotalElements());
//    }
//
    @GetMapping
    public Page<PessoaDTO> recuperarTodos(final Pageable pageable, @RequestParam(required = false)  String termo) {
        final Page<Pessoa>  page = pessoaService.recuperarTodosPorTermo(termo, pageable);
        return new PageImpl<>((List<PessoaDTO>) pessoaMapper.toDTO(page.getContent()), pageable, page.getTotalElements());
    }

    @GetMapping("/{id}")
    public PessoaDTO recuperarPorId(@PathVariable Long id) {
        return pessoaService
                .recuperarPorId(id)
                .map(pessoaMapper::toDTO)
                .orElseThrow(RuntimeException::new);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable Long id) {
        pessoaService.excluirPorId(id);
    }
}