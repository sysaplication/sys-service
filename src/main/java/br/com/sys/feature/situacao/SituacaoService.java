package br.com.sys.feature.situacao;

import br.com.sys.core.service.GenericService;
import br.com.sys.shared.model.Situacao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Service
@Transactional
public class SituacaoService extends GenericService<Situacao, Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
}
