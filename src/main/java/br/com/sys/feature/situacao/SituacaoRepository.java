package br.com.sys.feature.situacao;

import br.com.sys.core.repository.SysRepository;
import br.com.sys.shared.model.Situacao;

interface SituacaoRepository extends SysRepository<Situacao, Long> {
    
}