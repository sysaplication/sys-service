package br.com.sys.feature.situacao;

import br.com.sys.shared.dto.SituacaoDTO;
import br.com.sys.shared.dto.UFDTO;
import br.com.sys.shared.mapper.SituacaoMapper;
import br.com.sys.shared.model.Situacao;
import br.com.sys.shared.model.UF;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/situacoes")
class SituacaoController {

    private final SituacaoService situacaoService;

    private final SituacaoMapper situacaoMapper;

    public SituacaoController(SituacaoService situacaoService, SituacaoMapper situacaoMapper) {
        this.situacaoService = situacaoService;
        this.situacaoMapper = situacaoMapper;
    }

    @PostMapping
    public void salvar(@RequestBody SituacaoDTO situacaoDTO) {
        situacaoService.salvarOuAlterar(situacaoMapper.toEntity(situacaoDTO));
    }

    @PutMapping
    public void alterar(@RequestBody SituacaoDTO situacaoDTO) {
        situacaoService.salvarOuAlterar(situacaoMapper.toEntity(situacaoDTO));
    }

    @GetMapping
    public Page<SituacaoDTO> recuperarTodos(final Pageable pageable) {
        final Page<Situacao>  page = situacaoService.recuperarTodos(pageable);
        return new PageImpl<>((List<SituacaoDTO>) situacaoMapper.toDTO(page.getContent()), pageable, page.getTotalElements());
    }

    @GetMapping("/{id}")
    public SituacaoDTO recuperarPorId(@PathVariable Long id) {
        return situacaoService
                .recuperarPorId(id)
                .map(situacaoMapper::toDTO)
                .orElseThrow(RuntimeException::new);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable Long id) {
        situacaoService.excluirPorId(id);
    }
}