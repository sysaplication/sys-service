package br.com.sys.core.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DataUtil {


	public static String instantToString(Instant instant) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").withZone(ZoneId.systemDefault());
		return formatter.format(instant);
	}

	public static LocalDate retornarData() {
		return LocalDate.now();
	}

}
