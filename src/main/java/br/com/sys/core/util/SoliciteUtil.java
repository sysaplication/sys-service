package br.com.sys.core.util;

import java.util.HashMap;

public class SoliciteUtil {

	private static HashMap<Integer, String> mapaEnderecos = new HashMap<Integer, String>();

	public static String retornaEnderecoIcon(int posicao) {

		mapaEnderecos.put(1, "build/styles/ico/servico_domestico.ico");
		mapaEnderecos.put(2, "build/styles/ico/servico_eletrico.ico");
		mapaEnderecos.put(3, "build/styles/ico/servico_hidraulico.ico");
		mapaEnderecos.put(4, "build/styles/ico/servico_chaveiro.ico");
		mapaEnderecos.put(5, "build/styles/ico/servico_pintura.ico");
		mapaEnderecos.put(6, "build/styles/ico/servico_frete.ico");

		return mapaEnderecos.get(posicao);

	}
	
	public static boolean isNull(Object objeto) {
		return objeto == null;
	}
	
	public static boolean isNotNull(Object objeto) {
		return !isNull(objeto);
	}
}
