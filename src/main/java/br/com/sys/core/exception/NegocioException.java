package br.com.sys.core.exception;

import java.io.Serializable;

public class NegocioException extends RuntimeException implements Serializable{

	private static final long serialVersionUID = 1436770375628258034L;
	
	private String msg;
	
	public NegocioException() {
		super();
	}
	
	public NegocioException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMessage() {
		return msg;
	}
	
	public NegocioException(String mensagem, Exception ex) {
		super(mensagem, ex);
		this.msg = mensagem;
	}
}
