package br.com.sys.core.service;

import br.com.sys.core.exception.SysException;
import br.com.sys.core.model.BaseEntity;
import br.com.sys.shared.model.Pessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface IGenericService<T extends BaseEntity, ID> extends Serializable {

    public Stream<T> recuperarTodos() throws SysException;

    public Page<T> recuperarTodos(Pageable pageable) throws SysException;

    public Optional<T> recuperarPorId(ID id) throws SysException;

    public Optional<T> salvarOuAlterar(T entity) throws SysException;

    public void excluirPorId(ID id) throws SysException;

}
