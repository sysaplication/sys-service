package br.com.sys.core.service;

import br.com.sys.core.exception.SysException;
import br.com.sys.core.model.BaseEntity;
import br.com.sys.core.repository.SysRepository;
import br.com.sys.shared.model.Pessoa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Transactional
public abstract class GenericService<T extends BaseEntity, ID> implements IGenericService<T, ID>{

    private static final long serialVersionUID = 1L;

    @Autowired
    private SysRepository<T, Long> repository;
    
    @PersistenceContext(name = "primary")
    private EntityManager entityManager;
    
    @Override
    @Transactional
    public Stream<T> recuperarTodos() throws SysException {
    	return toStream(repository.findAll());
    }

    @Override
    @Transactional
    public Page<T> recuperarTodos(Pageable pageable) throws SysException {
        return repository.findAll(pageable);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Optional<T> salvarOuAlterar(T entity) throws SysException {
    	T entidadeInserir = Optional.of(entity).get();
        return Optional.of(repository.save(entidadeInserir));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void excluirPorId(ID id) throws SysException {
        Optional<T> entity = recuperarPorId(id);
        entity.ifPresent(t -> {
            repository.delete(entity.get());
        });
    }

	@Override
    @Transactional
	public Optional<T> recuperarPorId(ID id) throws SysException {
		return Optional.of(repository.getOne((Long)(Optional.of(id).get())));
	}

    protected Stream<T> toStream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
