package br.com.sys.core.mapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

@Mapper(componentModel = "spring")
public class LocalDateTimeStringMapper {

    public String asString(LocalDateTime data) {
        return !StringUtils.isEmpty(data) ? DateTimeFormatter.ofPattern( "dd/MM/yyyy HH:mm" ).format( data ) : null;
    }

    public LocalDateTime asDate(String data) {
        return !StringUtils.isEmpty(data) ? LocalDateTime.parse( data, DateTimeFormatter.ofPattern( "dd/MM/yyyy HH:mm" ) ) : null;
    }
}