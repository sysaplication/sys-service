package br.com.sys.core.mapper;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

import org.mapstruct.Mapper;

import br.com.sys.core.util.SoliciteUtil;

@Mapper(componentModel = "spring")
public class TelefoneMapper {

    public String asString(Long telefone) {
        MaskFormatter mf;
        try {
            mf = new MaskFormatter("(##)#####-####");
            mf.setValueContainsLiteralCharacters(false);
            return SoliciteUtil.isNotNull(telefone)  ? mf.valueToString(telefone) : null;
        } catch (ParseException ex) {
            return telefone.toString();
        }
    }

    public Long asLong(String telefone) {
        return SoliciteUtil.isNotNull(telefone) ? new Long(telefone.replaceAll("[^\\d.]", "")) : null;
    }
}