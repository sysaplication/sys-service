package br.com.sys.core.mapper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

@Mapper(componentModel = "spring")
public class LocalDateStringMapper {

    public String asString(LocalDate data) {
        return !StringUtils.isEmpty(data) ? DateTimeFormatter.ofPattern( "dd/MM/yyyy" ).format( data ) : null;
    }

    public LocalDate asDate(String data) {
        return !StringUtils.isEmpty(data) ? java.time.LocalDate.parse( data, DateTimeFormatter.ofPattern( "dd/MM/yyyy" ) ) : null;
    }
}