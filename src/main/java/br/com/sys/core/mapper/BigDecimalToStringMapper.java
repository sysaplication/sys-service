package br.com.sys.core.mapper;

import java.math.BigDecimal;

import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

@Mapper(componentModel = "spring")
public class BigDecimalToStringMapper {

    public String asString(BigDecimal data) {
        return !StringUtils.isEmpty(data) ? data.setScale(2).toPlainString().replaceAll("\\.",",") : null;
    }

    public BigDecimal asBigDecimal(String data) {
    	if (!StringUtils.isEmpty(data)) {
    	    String valor = data.replaceAll(",","\\.");
       	 	return new BigDecimal(valor).movePointLeft(2);
    	} else {
    		return new BigDecimal("0.00");
    	}
    }
}