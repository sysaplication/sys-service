package br.com.sys.core.mapper;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

@Mapper(componentModel = "spring")
public class LocalTimeStringMapper {

    public String asString(LocalTime hora) {
        return !StringUtils.isEmpty(hora) ? DateTimeFormatter.ofPattern( "H:mm" ).format( hora ): null;
    }

    public LocalTime asDate(String hora) {
        return !StringUtils.isEmpty(hora) ? java.time.LocalTime.parse( hora, DateTimeFormatter.ofPattern( "H:mm" ) ) : null;
    }
}