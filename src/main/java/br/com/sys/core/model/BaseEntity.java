package br.com.sys.core.model;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@SuppressWarnings("serial")
@MappedSuperclass
@Data
public abstract class BaseEntity<ID> implements Serializable {

}

