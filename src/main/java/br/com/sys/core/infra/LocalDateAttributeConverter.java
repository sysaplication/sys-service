package br.com.sys.core.infra;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.sys.core.util.SoliciteUtil;

@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {
	
    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
    	return (SoliciteUtil.isNull(locDate) ? null : Date.valueOf(locDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date sqlDate) {
    	return (SoliciteUtil.isNull(sqlDate) ? null : sqlDate.toLocalDate());
    }
}