package br.com.sys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Boot{

	public static void main(String[] args) {
		SpringApplication.run(Boot.class, args);
	}


}
