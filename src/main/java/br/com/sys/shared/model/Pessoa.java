package br.com.sys.shared.model;

import br.com.sys.core.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@EqualsAndHashCode(callSuper= false, of = "id")
@ToString
@Table(name = "pessoa")
public class Pessoa extends BaseEntity {

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "pessoa_id";

    @Id
    @SequenceGenerator(name = "pessoa", sequenceName = "seq_pessoa", allocationSize = 1)
    @GeneratedValue(generator = "pessoa", strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = "nome", length = 70, nullable = false)
    private String nome;

    @Column(name = "celular", length = 12)
    private String celular;

    @Column(name = "data_nascimento", nullable = false)
    private LocalDate dataNascimento;

    @ManyToOne(cascade=CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name=UF.COL_ID)
    private UF uf;

    @ManyToOne(cascade=CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name=Situacao.COL_ID)
    private Situacao situacao;
}