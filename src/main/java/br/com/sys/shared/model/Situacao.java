package br.com.sys.shared.model;

import br.com.sys.core.model.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper= false, of = "id")
@ToString
@Table(name = "situacao")
public class Situacao extends BaseEntity {

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "situacao_id";

    @Id
    @SequenceGenerator(name = "situacao", sequenceName = "seq_situacao", allocationSize = 1)
    @GeneratedValue(generator = "situacao", strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = "descricao", length = 25, unique = true, nullable = false)
    private String descricao;
}
