package br.com.sys.shared.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import br.com.sys.core.model.BaseEntity;


@Entity
@Data
@EqualsAndHashCode(callSuper= false, of = "id")
@ToString
@Table(name = "USR_USUARIO")
@AllArgsConstructor
public class Usuario extends BaseEntity<String> {

	private static final long serialVersionUID = 1L;

	public static final String COL_ID = "USR_ID";

	public Usuario (){}
	
	public Usuario (String id){
		this.id = id;
	}
	
    @Id
    @Column(name = COL_ID)
    private String id;
    
    @Column(name = "USR_ST_NOME")
    @Size(max = 200)
    @NotNull
    private String nome;

    @Column(name = "USR_ST_SOBRENOME", columnDefinition = "")
    @Size(max = 200)
    private String sobrenome;

    @Column(name = "USR_ST_EMAIL", unique = true, updatable = false)
    @Size(max = 150)
    @NotNull
    private String email;
    
    @Column(name = "USR_NU_TELEFONE")
    private Long telefone;

    @Column(name = "USR_NU_NOTA")
    private Double nota;

    @Column(name = "USR_ST_ONE_SIGNAL_ID")
    private String oneSignalId;
    
    @Column(name = "USR_ST_PUSH_TOKEN")
    private String pushToken;

    @ManyToMany(cascade = {CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinTable(name = "ROU_ROLE_USUARIO", joinColumns = {
            @JoinColumn(name = Usuario.COL_ID) }, inverseJoinColumns = {
            @JoinColumn(name = Role.COL_ID) })
    private List<Role> roles;

}
