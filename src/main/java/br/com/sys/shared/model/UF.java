package br.com.sys.shared.model;

import br.com.sys.core.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper= false, of = "id")
@ToString
@Table(name = "uf")
public class UF extends BaseEntity {

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "uf_id";

    @Id
    @SequenceGenerator(name = "uf", sequenceName = "seq_uf", allocationSize = 1)
    @GeneratedValue(generator = "uf", strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = "nome", length = 60, unique = true, nullable = false, updatable = false)
    private String descricao;

    @Column(name = "sigla", length = 2, unique = true, nullable = false, updatable = false)
    private String sigla;
}
