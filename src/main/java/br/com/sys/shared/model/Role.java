package br.com.sys.shared.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false, of = "id")
@ToString
@Table(name = "ROL_ROLE")
public class Role {

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "ROL_ID";

    @Id
    @SequenceGenerator(name = "role", sequenceName = "seq_role", allocationSize = 1)
    @GeneratedValue(generator = "role", strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = "ROL_ST_NOME")
    private String nome;

    @ManyToMany(mappedBy = "roles")
    private List<Usuario> usuarios;

}