package br.com.sys.shared.mapper;


import br.com.sys.core.mapper.LocalDateStringMapper;
import br.com.sys.shared.dto.PessoaDTO;
import br.com.sys.shared.dto.SituacaoDTO;
import br.com.sys.shared.model.Pessoa;
import br.com.sys.shared.model.Situacao;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {UFMapper.class, SituacaoMapper.class, LocalDateStringMapper.class})
public interface PessoaMapper extends BaseMapper<Pessoa, PessoaDTO> {

}
