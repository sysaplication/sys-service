package br.com.sys.shared.mapper;


import br.com.sys.shared.dto.UFDTO;
import br.com.sys.shared.model.UF;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UFMapper extends BaseMapper<UF, UFDTO>{

}
