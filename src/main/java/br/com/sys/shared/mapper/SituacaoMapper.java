package br.com.sys.shared.mapper;


import br.com.sys.shared.dto.SituacaoDTO;
import br.com.sys.shared.model.Situacao;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SituacaoMapper extends BaseMapper<Situacao, SituacaoDTO>{

}
