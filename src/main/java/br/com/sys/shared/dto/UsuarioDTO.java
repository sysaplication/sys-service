package br.com.sys.shared.dto;

import lombok.Data;

@Data
public class UsuarioDTO {

    private String id;

    private String nome;

    private String sobrenome;

    private String email;

    private String telefone;

    private Double nota;

    private String pushToken;

}
