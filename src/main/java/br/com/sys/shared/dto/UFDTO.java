package br.com.sys.shared.dto;

import lombok.Data;

@Data
public class UFDTO {

    private Long id;

    private String descricao;

    private String sigla;
}
