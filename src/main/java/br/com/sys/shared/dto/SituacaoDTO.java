package br.com.sys.shared.dto;

import lombok.Data;

@Data
public class SituacaoDTO {

    private Long id;

    private String descricao;

}
