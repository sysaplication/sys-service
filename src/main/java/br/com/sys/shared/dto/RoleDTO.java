package br.com.sys.shared.dto;

import lombok.Data;

@Data
public class RoleDTO {

    private String nome;
}
