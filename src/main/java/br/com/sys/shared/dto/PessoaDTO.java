package br.com.sys.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PessoaDTO {

    public PessoaDTO(String nome) {
        this.nome = nome;
    }
    private Long id;

    private String nome;

    private String celular;

    private String dataNascimento;

    private UFDTO uf;

    private SituacaoDTO situacao;
}