FROM openjdk:8-jre-alpine

RUN adduser -D -g '' java

ADD /target/*.jar /sys-service.jar

VOLUME /tmp

RUN sh -c 'touch /sys-service.jar'

USER java

EXPOSE 8080

CMD java -jar /sys-service.jar
